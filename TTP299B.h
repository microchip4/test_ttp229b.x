/*******************************************************************************
 *                  Library: TTP299B
 *******************************************************************************
 * FileName:        TTP299B.h
 * Processor:       PICxxxxxx
 * Complier:        XC8 v2.10
 * Author:          Brandon Ruiz Vasquez
 * Email:           Branrv64@gmail.com
 * Description:     Touch button module that allows up to 16 buttons
 *                  Implements a 2-wire communication.
 *                  Tested on board HW-136
 * Board mod:       Five connections have been created in order for the board
 *                  to function efficiently
 *                  16 Buttons Mode     TP2
 *                  Multi Keys Mode     TP3, TP4
 *                  64Hz Sampling Rate  TP5
 *                  2ms Wake Up Rate    TP6
 *      *   6   5   4
 * TP       |   |   |
 *      *   6   5   4
 * 
 *      *   *   2   3
 * TP           |   |
 *      *   *   2   3
 ******************************************************************************/

#ifndef TTP299B_H
#define	TTP299B_H


/********** PORT CONFIGRATION ***************************************/
#define TTP299B_TRIS_SCL    TRISBbits.TRISB0
#define TTP299B_TRIS_SDO    TRISBbits.TRISB1

#define TTP299B_PORT_SCL    LATBbits.LATB0
#define TTP299B_PORT_SDO    PORTBbits.RB1

/********** Function Prototypes *************************************/

void TTP299B_init();
int TTP299B_getAll();
char TTP299B_getButton(char);

#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif

#endif	/* TTP299B_H */

